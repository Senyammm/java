//Password = 0000
import Dop.Cabinet;
import Dop.Kinoteatr;
import Dop.Session;
import Dop.Utils;

import java.util. *;

public class lab_3 {
    public static void main(String[] args) {
        System.out.println("Приветствую вас в нашей сети кинотеатров !");
        // Cоздание кинотеатров в ручную!
        Cabinet[] cabinets = new Cabinet[3];
        Cabinet cabinet1 = new Cabinet(1,6, 7);
        Cabinet cabinet2 = new Cabinet(2,7, 8);
        Cabinet cabinet3 = new Cabinet(3,8, 9);
        cabinets[0] = cabinet1;
        cabinets[1] = cabinet2;
        cabinets[2] = cabinet3;


        Kinoteatr[] kinoteatrs = new Kinoteatr[2];
        Kinoteatr tut = new Kinoteatr("a", cabinets);
        Kinoteatr tam = new Kinoteatr("o", cabinets);
        kinoteatrs[0] = tut;
        kinoteatrs[1] = tam;

        Session[] sessions = new Session[1];
        Session session1 = new Session(kinoteatrs[0], "Test_film", kinoteatrs[0].getCabinets()[0], new Date(), new Date());
        sessions[0] = session1;



        boolean run = true;
        boolean admin = false;
        boolean user = false;
        String adminPassword = "0000";

        while (run) {
            System.out.println("Представтесь!");
            System.out.println("1 - Admin");
            System.out.println("2 - User");
            System.out.println("3 - Выйти!");
            int role_1 = Utils.correctInputInt();

            switch (role_1) {
                case 1:
                    System.out.println("Введите пароль для admin:");
                    String password = Utils.correctInputString();
                    if(password.equals(adminPassword)) {
                        admin = true;
                    } else {
                        System.out.println("Неправильный пароль!");
                        break;
                    }
                    break;
                case 2:
                    user = true;
                    break;
                case 3:
                    run = false;
                    break;
                default:
                    System.out.println("Выбранной команды не существует!");
                    break;
            }

            while (admin) {
                System.out.println("Выберите действие!");
                System.out.println("1 - Создать кинотетатр");
                System.out.println("2 - Добавить кинозал к выбранному кинотеатру");
                System.out.println("3 - Вывести полную информацию о кинотеатрах");
                System.out.println("4 - Добавить сеанс ");
                System.out.println("5 - Все сеансы ");
                System.out.println("6 - К выбору роли!");
                System.out.println("-----------------");

                int role_2 = Utils.correctInputInt();

                switch (role_2) {
                    case 1:
                        kinoteatrs = position_1(kinoteatrs);
                        break;
                    case 2:
                        kinoteatrs = position_2(kinoteatrs);
                        break;
                    case 3:
                        position_3(kinoteatrs);
                        break;
                    case 4:
                        sessions = position_4(kinoteatrs, sessions);
                        break;
                    case 5:
                        position_5(sessions);
                        break;
                    case 6:
                        admin = position_6(admin);
                        break;
                    default:
                        System.out.println("Выбранной команды не существует!");
                        break;
                }
            }

            while (user) {
                System.out.println("Что вы хотите сделать?");
                System.out.println("1 - Узнать ближайший сеанс");
                System.out.println("2 - Список сеансов");
                System.out.println("3 - Купить билет");
                System.out.println("4 - К выбору роли!");

                int role_2 = Utils.correctInputInt();

                switch (role_2) {
                    case 1:
                        Utils.printSession(sessions[0]);
                        break;
                    case 2:
                        System.out.println("И так, что мне удалось найти");
                        for (Session session : sessions) {
                            Utils.printSession(session);
                        }
                        break;
                    case 3:
                        sessions = BuyTicket(sessions);
                        break;
                    case 4:
                        user = position_6(user);
                        break;
                    default:
                        System.out.println("Выбранной команды не существует!");
                        break;
                }
            }
        }
    }

    public static Kinoteatr[] position_1(Kinoteatr [] kinoteatrs)
    {
        System.out.println("Создаем кинотеатр");
        kinoteatrs = Utils.upLengthK(kinoteatrs);
        Kinoteatr kino = Create_kinoteatr();
        kinoteatrs[kinoteatrs.length - 1] = kino;
        System.out.println("Кинотеатр " + kino.getName() + " создан!");
        System.out.println("-----------------");
        return kinoteatrs;
    }

    public static Kinoteatr[] position_2(Kinoteatr [] kinoteatrs)
    {
        System.out.println("Выберите кинотеатр : ");
        Utils.printKinoteatrs(kinoteatrs);
        int role_2 = Utils.correctInputInt();
        Kinoteatr kinoteatr = kinoteatrs[role_2-1];
        kinoteatr.setCabinets(Utils.upLengthC(kinoteatr.getCabinets()));
        System.out.println("В Кинотеатре " + kinoteatr.getName() + " добавлен новый кинозал!");
        System.out.println("-----------------");
        return kinoteatrs;
    }

    public static void position_3(Kinoteatr [] kinoteatrs)
    {
        System.out.println("И так, что мне удалось найти :");
        for (Kinoteatr kinoteatr : kinoteatrs) {
            kinoteatr.getInfo();
        }
        System.out.println("-----------------");
    }

    public static Session[] position_4(Kinoteatr [] kinoteatrs, Session [] sessions)
    {
        Scanner scanner = new Scanner(System.in);
        sessions = Utils.upLengthS(sessions);

        System.out.println("Выберите кинотеатр:");
        Utils.printKinoteatrs(kinoteatrs);
        int id_kinoteatr = Utils.correctInputInt();
        id_kinoteatr -= 1;
        System.out.println(kinoteatrs[id_kinoteatr].getName());

        System.out.println("Выберите зал:");
        Utils.printCabinets(kinoteatrs[id_kinoteatr]);
        int id_cabinet = Utils.correctInputInt();
        id_cabinet -= 1;
        System.out.println(kinoteatrs[id_kinoteatr].getName());

        System.out.println("Введите название фильма");
        String title = Utils.correctInputString();

        System.out.println("Введите время начала фильма (<yyyy-mm-dd hh:mm:ss>)");
        Date start_f = Utils.ReadDate();

        System.out.println("Введите время окончания фильма(<yyyy-mm-dd hh:mm:ss>)");
        Date end_f = Utils.ReadDate();


        Session session = new Session(kinoteatrs[id_kinoteatr],
                title,
                kinoteatrs[id_kinoteatr].getCabinets()[id_cabinet],
                start_f,
                end_f);
        sessions[sessions.length - 1] = session;
        sessions = Utils.sortTime(sessions);
        return sessions;
    }

    public static void position_5(Session [] sessions)
    {
        System.out.println("Что мне удалось найти");
        for (Session session : sessions)
        {
            Utils.printSession(session);
        }
    }

    public static boolean position_6(boolean run)
    {
        System.out.println("До скорых встреч!");
        run = false;
        return run;
    }
    public static Session[] BuyTicket(Session [] sessions) {
        boolean flag = true;
        int id_row = 0;
        int id_place = 0;
        System.out.println("//////////");
        System.out.println("Выберите номер сеанса");
        System.out.println("//////////");
        for (int id_session = 0; id_session < sessions.length; id_session++) {
            System.out.println("СЕАНС №" + (id_session + 1));
            Utils.printSession(sessions[id_session]);
        }
        int id_session = Utils.correctInputInt();
        id_session -= 1;
        if ((id_session > sessions.length - 1 || id_session < 0) && flag)
        {
            flag = false;
            System.out.println("Выбранного сеанса не существует");
        }
        else {
            System.out.println("Отлично!");
            System.out.println("Выберите ряд!");
            id_row = Utils.correctInputInt();
            if ((id_row > sessions[id_session].getCabinet().getRow() || id_row - 1 < 0) && flag) {
                flag = false;
                System.out.println("Выбранного ряда не существует");
            }
            else {
                System.out.println("Выберите место!");
                id_place = Utils.correctInputInt();
                if ((id_place > sessions[id_session].getCabinet().getPlace_in_row() || id_place - 1 < 0) && flag) {
                    flag = false;
                    System.out.println("Выбранного места не существует");
                }
            }
        }

        if (flag)
        {
            sessions[id_session].getCabinet().setSeat(id_row - 1, id_place - 1);
            System.out.println("Вы приобрели билет на сеанс - " + sessions[id_session].getTitle());
            System.out.println("Начало сеанса - " + sessions[id_session].getStart_f());
            System.out.println("Кинотеатр - " + sessions[id_session].getKinoteatr().getName());
            System.out.println("Зал - №" + sessions[id_session].getCabinet().getNumber());
            System.out.println("Ваше место:)");
            System.out.println("////////");
            System.out.println("Ряд - " + id_row);
            System.out.println("Место - " + id_place);
            System.out.println("/////////");
            System.out.println("Приятного просмотра!");
            System.out.println();
        }
        else
        {
            System.out.println();
            System.out.println("Неправильный ввод данных");
            System.out.println("Попробуйте еще раз!");
            System.out.println();
        }
        return sessions;
    }

    public static Kinoteatr Create_kinoteatr() {
        System.out.println("Введите название кинотетатра");
        Scanner scanner = new Scanner(System.in);
        String name = scanner.nextLine();
        System.out.println("Введите количество залов : ");
        int number_of_cabinets = Utils.correctInputInt();
        Cabinet [] cabinets = new Cabinet[number_of_cabinets];
        for (int i = 0; i < number_of_cabinets; i++) {
            System.out.println("Зал № " + (i + 1));
            System.out.println("Введите количество рядов в зале");
            int rows = Utils.correctInputInt();
            System.out.println("Введите количество мест в ряде");
            int numbers_chair = Utils.correctInputInt();
            Cabinet cabinet = new Cabinet((i+1), rows, numbers_chair);
            cabinets[i] = cabinet;
        }
        Kinoteatr kinoteatr = new Kinoteatr(name, cabinets);
        return kinoteatr;
    }
}
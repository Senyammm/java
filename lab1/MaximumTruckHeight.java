import java.util.*;

public class MaximumTruckHeight {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Введите количество дорог:");
        int numRoads = scanner.nextInt();

        // Создаем граф дорог
        Map<String, List<Road>> graph = new HashMap<>();
        for (int i = 0; i < numRoads; i++) {
            System.out.println("Введите название дороги:");
            String roadName = scanner.next();
            System.out.println("Введите количество туннелей на дороге:");
            int numTunnels = scanner.nextInt();

            List<Road> tunnels = new ArrayList<>();
            for (int j = 0; j < numTunnels; j++) {
                System.out.println("Введите высоту туннеля:");
                int tunnelHeight = scanner.nextInt();
                tunnels.add(new Road(tunnelHeight));
            }

            graph.put(roadName, tunnels);
        }

        System.out.println("Введите начальную и конечную точки:");
        String start = scanner.next();
        String end = scanner.next();

        // Используем алгоритм Дейкстры для поиска кратчайшего пути
        PriorityQueue<Node> pq = new PriorityQueue<>();
        pq.offer(new Node(start, Integer.MAX_VALUE));
        Set<String> visited = new HashSet<>();
        Map<String, Integer> minHeight = new HashMap<>();
        minHeight.put(start, Integer.MAX_VALUE);

        while (!pq.isEmpty()) {
            Node node = pq.poll();
            String curr = node.name;
            int currMinHeight = node.minHeight;

            if (visited.contains(curr)) {
                continue;
            }

            visited.add(curr);
            minHeight.put(curr, currMinHeight);

            if (curr.equals(end)) {
                break;
            }

            for (Road tunnel : graph.get(curr)) {
                int nextMinHeight = Math.min(currMinHeight, tunnel.height);
                String next = tunnel.name;

                if (!visited.contains(next)) {
                    pq.offer(new Node(next, nextMinHeight));
                }
            }
        }

        // Выводим минимальную высоту туннеля на всем пути
        System.out.println("Максимальная высота грузовика: " + minHeight.get(end));
    }

    static class Road {
        String name;
        int height;

        public Road(int height) {
            this.height = height;
        }
    }

    static class Node implements Comparable<Node> {
        String name;
        int minHeight;

        public Node(String name, int minHeight) {
            this.name = name;
            this.minHeight = minHeight;
        }

        @Override
        public int compareTo(Node other) {
            return Integer.compare(other.minHeight, this.minHeight);
        }
    }
}

import java.util.Scanner;

public class DoublyEvenNumber {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Введите трехзначное число:");
        int number = scanner.nextInt();

        // Проверяем, является ли число трехзначным
        if (number < 100 || number > 999) {
            System.out.println("Введенное число не является трехзначным.");
            return;
        }

        // Вычисляем сумму цифр
        int sum = 0;
        int temp = number;
        while (temp != 0) {
            sum += temp % 10;
            temp /= 10;
        }

        // Вычисляем произведение цифр
        int product = 1;
        temp = number;
        while (temp != 0) {
            product *= temp % 10;
            temp /= 10;
        }

        // Проверяем, является ли сумма и произведение четными числами
        if (sum % 2 == 0 && product % 2 == 0) {
            System.out.println("Число является дважды четным.");
        } else {
            System.out.println("Число не является дважды четным.");
        }
    }
}

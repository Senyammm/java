import java.util.Scanner;

public class AlternatingSum {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Введите значение p (количество чисел):");
        int p = scanner.nextInt();

        // Проверяем, что p > 0
        if (p <= 0) {
            System.out.println("Число p должно быть больше нуля.");
            return;
        }

        System.out.println("Введите " + p + " чисел:");

        // Вычисляем знакочередующуюся сумму
        int alternatingSum = 0;
        for (int i = 1; i <= p; i++) {
            int number = scanner.nextInt();

            // Определяем знак числа в зависимости от четности индекса
            int sign = (i % 2 == 0) ? -1 : 1;

            alternatingSum += sign * number;
        }

        // Выводим знакочередующуюся сумму ряда
        System.out.println("Знакочередующаяся сумма ряда: " + alternatingSum);
    }
}

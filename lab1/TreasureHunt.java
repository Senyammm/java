import java.util.Scanner;

public class TreasureHunt {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int currentX = 0;
        int currentY = 0;

        System.out.println("Введите координаты клада (x, y):");
        int treasureX = scanner.nextInt();
        int treasureY = scanner.nextInt();

        System.out.println("Введите указания карты (направление и количество шагов):");
        while (true) {
            String direction = scanner.next();
            if (direction.equals("стоп")) {
                break;
            }
            int steps = scanner.nextInt();

            // Обновляем текущие координаты в соответствии с указанием карты
            if (direction.equals("север")) {
                currentY += steps;
            } else if (direction.equals("юг")) {
                currentY -= steps;
            } else if (direction.equals("запад")) {
                currentX -= steps;
            } else if (direction.equals("восток")) {
                currentX += steps;
            }
        }

        // Вычисляем расстояние до клада
        int distance = Math.abs(currentX - treasureX) + Math.abs(currentY - treasureY);

        System.out.println("Минимальное количество шагов до клада: " + distance);
    }
}

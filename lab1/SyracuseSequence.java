
public class SyracuseSequence {
    public static void main(String[] args) {
        int n = 11;
        int steps = 0;
        System.out.print(n + " ");
        while (n != 1) {
            if (n % 2 == 0) {
                n /= 2;
            } else {
                n = 3 * n + 1;
            }
            System.out.print(n + " ");
            steps++;
        }
        System.out.println("\nКоличество шагов: " + steps);
    }
}



public class Main
{
    public static void main(String[] args)
    {
        int[][] matrix =
                {
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9}
        };

        int n = matrix.length;
        int[][] result = new int[n][n];

        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < n; j++)
            {
                result[i][j] = matrix[n - j - 1][i];
            }
        }

        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < n; j++)
            {
                System.out.print(result[i][j] + " ");
            }
            System.out.println();
        }
    }
}
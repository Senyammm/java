import java.util.Arrays;

class Main
{
    // Функция для объединения двух отсортированных массивов X[] и Y[]
    public static int[] merge(int[] X, int[] Y)
    {
        int k = 0, i = 0, j = 0;
        int[] aux = new int[X.length + Y.length];

        // пока есть элементы в первом и втором массивах
        while (i < X.length && j < Y.length)
        {
            if (X[i] <= Y[j])
            {
                aux[k++] = X[i++];
            }
            else
            {
                aux[k++] = Y[j++];
            }
        }

        // копируем оставшиеся элементы
        while (i < X.length)
        {
            aux[k++] = X[i++];
        }

        while (j < Y.length)
        {
            aux[k++] = Y[j++];
        }

        return aux;
    }

    public static void main (String[] args)
    {
        int[] X = { 1, 3, 6, 8, 10 };
        int[] Y = { 5, 7, 9, 11 };

        int[] aux = merge(X, Y);

        System.out.println("First Array : " + Arrays.toString(X));
        System.out.println("Second Array: " + Arrays.toString(Y));
        System.out.println("After Merge : " + Arrays.toString(aux));
    }
}
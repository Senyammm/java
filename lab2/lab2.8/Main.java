public class Main {
    public static void main(String[] args) {
        int[][] array = {
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9}
        };

        int[][] rotatedArray = rotateArray(array);
        printArray(rotatedArray);
    }

    public static int[][] rotateArray(int[][] array) {
        int rows = array.length;
        int cols = array[0].length;

        int[][] rotatedArray = new int[cols][rows];

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                rotatedArray[cols - j - 1][i] = array[i][j];
            }
        }

        return rotatedArray;
    }

    public static void printArray(int[][] array) {
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                System.out.print(array[i][j] + " ");
            }
            System.out.println();
        }
    }
}

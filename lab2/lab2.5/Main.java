class Main
{
    // Наивный метод поиска пары в массиве с заданной суммой
    public static void findPair(int[] nums, int target)
    {
        // рассмотрим каждый элемент, кроме последнего
        for (int i = 0; i < nums.length - 1; i++)
        {
            // начинаем с i-го элемента до последнего элемента
            for (int j = i + 1; j < nums.length; j++)
            {
                // если искомая сумма найдена, выводим ее
                if (nums[i] + nums[j] == target)
                {
                    System.out.printf("Pair found (%d, %d)", nums[i], nums[j]);
                    return;
                }
            }
        }

        // доходим сюда, если пара не найдена
        System.out.println("Pair not found");
    }

    public static void main (String[] args)
    {
        int[] nums = { 8, 7, 2, 5, 3, 1 };
        int target = 12;

        findPair(nums, target);
    }
}

import java.util.HashSet;

public class LongestSubstringWithoutRepeatingCharacters {
    public static void main(String[] args) {
        String s = "aboiuyvgu"; // Пример строки

        int maxLength = 0; // Длина наибольшей подстроки без повторяющихся символов
        int start = 0; // Начальный индекс наибольшей подстроки
        int end = 0; // Конечный индекс наибольшей подстроки

        HashSet<Character> set = new HashSet<>(); // Хэш-множество для проверки повторяющихся символов

        while (end < s.length()) {
// Если текущий символ уже присутствует в множестве, удалить все символы до него и увеличить начальный индекс
            if (set.contains(s.charAt(end))) {
                set.remove(s.charAt(start));
                start++;
            } else {
// Иначе, добавить символ в множество и увеличить конечный индекс
                set.add(s.charAt(end));
                end++;

// Обновить наибольшую длину подстроки, если текущая длина больше
                maxLength = Math.max(maxLength, end - start);
            }
        }

        System.out.println("Наибольшая подстрока без повторяющихся символов: " + s.substring(start, end));
        System.out.println("Длина наибольшей подстроки без повторяющихся символов: " + maxLength);
    }
}
class Main
{
    // Функция для нахождения максимальной суммы непрерывного подмассива
    // в заданном целочисленном массиве
    public static int kadane(int[] arr)
    {
        // сохраняет максимальный суммарный подмассив, найденный на данный момент
        int maxSoFar = 0;

        // сохраняет максимальную сумму подмассива, заканчивающегося на текущей позиции
        int maxEndingHere = 0;

        // обход заданного массива
        for (int i: arr)
        {
            // обновить максимальную сумму подмассива, "заканчивающегося" на индексе "i" (путем добавления
            // текущий элемент до максимальной суммы, заканчивающейся на предыдущем индексе 'i-1')
            maxEndingHere = maxEndingHere + i;

            // если максимальная сумма отрицательна, устанавливаем ее в 0 (что представляет
            // пустой подмассив)
            maxEndingHere = Integer.max(maxEndingHere, 0);

            // обновить результат, если текущая сумма подмассива окажется больше
            maxSoFar = Integer.max(maxSoFar, maxEndingHere);
        }

        return maxSoFar;
    }

    public static void main(String[] args)
    {
        int[] arr = { -3, 2, -5, 8, -4, 11, 5, -8, 9 };

        System.out.println("The sum of contiguous subarray with the " +
                "largest sum is " + kadane(arr));
    }
}
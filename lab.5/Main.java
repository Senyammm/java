package com.example.demo;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import javafx.stage.Stage;

public class Main extends Application {


    Controller a=new Controller();
    Button[] but = new Button[5];
    Label[][] lab = new Label[4][4];
    TextField ta=new TextField();
    Label scor=new Label("Счет:");

    @Override
    public void start(Stage primaryStage) throws Exception{
        a.zonelocat();
        try {

            heti();
            but[0] = new Button("наверх");
            but[1] = new Button("вниз");
            but[2] = new Button("влево");
            but[3] = new Button("вправо");
            but[4] = new Button("старт");
            but[0].setOnAction((ActionEvent arg0) -> up());
            but[1].setOnAction((ActionEvent arg0) -> down());
            but[2].setOnAction((ActionEvent arg0) -> left());
            but[3].setOnAction((ActionEvent arg0) -> right());
            but[4].setOnAction((ActionEvent arg0) -> begin());
            but[0].setPrefSize(100, 50);
            but[1].setPrefSize(100, 50);
            but[2].setPrefSize(100, 50);
            but[3].setPrefSize(100, 50);
            but[4].setPrefSize(200, 50);
            GridPane root = new GridPane();
            for (int i = 1; i < 5; i++) {
                for (int ii = 0; ii < 4; ii++) {
                    root.add(lab[i-1][ii], ii, i);
                }
            }
            root.add(scor,1,0);
            root.add(ta,2,0,2,1);
            root.add(but[0], 0, 5);
            root.add(but[1], 1, 5);
            root.add(but[2], 2, 5);
            root.add(but[3], 3, 5);
            root.add(but[4],1,6,2,1);
            root.setGridLinesVisible(true);
            Scene scene = new Scene(root, 400, 500);

            primaryStage.setScene(scene);
            primaryStage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void heti(){
        for (int i = 0; i < 4; i++) {
            for (int ii = 0; ii < 4; ii++) {
                lab[i][ii] = new Label(a.data[i][ii] + "");
                lab[i][ii].setPrefSize(100, 100);
                //lab[i][ii].setPadding(new Insets(40));
                lab[i][ii].setAlignment(Pos.CENTER);
                lab[i][ii].setFont(new Font(40));
            }
        }
    }

    void change() {
        for (int i = 0; i < 4; i++)
            for (int ii = 0; ii < 4; ii++){
                lab[i][ii].setText(a.data[i][ii] + "");
                if(a.data[i][ii]==0)
                    lab[i][ii].setStyle("-fx-background-color:#334455");
                else if(a.data[i][ii]==2)
                    lab[i][ii].setStyle("-fx-background-color:green");
                else if(a.data[i][ii]==4)
                    lab[i][ii].setStyle("-fx-background-color:yellow");
                else if(a.data[i][ii]==8)
                    lab[i][ii].setStyle("-fx-background-color:tomato");
                else if(a.data[i][ii]==16)
                    lab[i][ii].setStyle("-fx-background-color:gold");
                else if(a.data[i][ii]==32)
                    lab[i][ii].setStyle("-fx-background-color:cyan");
                else if(a.data[i][ii]==64)
                    lab[i][ii].setStyle("-fx-background-color:orange");
                else if(a.data[i][ii]==128)
                    lab[i][ii].setStyle("-fx-background-color:silver");
                else if(a.data[i][ii]==256)
                    lab[i][ii].setStyle("-fx-background-color:pink");
                else if(a.data[i][ii]==512)
                    lab[i][ii].setStyle("-fx-background-color:magenta");
                else if(a.data[i][ii]==1024)
                    lab[i][ii].setStyle("-fx-background-color:#ff1111");
                else
                    lab[i][ii].setStyle("-fx-background-color:red");
            }
    }

    void up() {
        a.up();
        change();
        ta.setText(a.score+"");
    }
    void down() {
        a.down();
        change();
        ta.setText(a.score+"");
    }
    void left() {
        a.left();
        change();
        ta.setText(a.score+"");
    }
    void right() {
        a.right();
        change();
        ta.setText(a.score+"");
    }
    void begin(){
        a.queding();
        a.queding();
        change();
        ta.setText(a.score+"");
    }


    public static void main(String[] args) {
        launch(args);
    }
}

package Figures;

public class Rook extends Figure{
    public Rook(String name, char color, int row, int col) {
        super(name, color, row, col);
    }
    public boolean canMove(int row, int col, int row1, int col1, Figure[][] board) {
        if (row == row1 && col != col1) {
            int deltaCol = (col1 - col) > 0 ? 1 : -1;
            int currentCol = col + deltaCol;
            while (currentCol != col1) {
                if (board[row][currentCol] != null) {
                    return false;
                }
                currentCol += deltaCol;
            }
            return true;
        } else if (row != row1 && col == col1) {
            int deltaRow = (row1 - row) > 0 ? 1 : -1;
            int currentRow = row + deltaRow;
            while (currentRow != row1) {
                if (board[currentRow][col] != null) {
                    return false;
                }
                currentRow += deltaRow;
            }
            return true;
        }
        return false;
    }
}
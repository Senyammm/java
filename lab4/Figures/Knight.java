package Figures;
public class Knight extends Figure{
    public Knight(String name, char color, int row, int col) {
        super(name, color, row, col);
    }
    public boolean canMove(int row, int col, int row1, int col1, Figure[][] board) {
        if ((Math.abs(row - row1)==1 && Math.abs(col - col1)==2) || (Math.abs(row - row1)==2 && Math.abs(col - col1)==1)){
            if (hasObstacles(row, col, row1, col1, board)) {
                return false;
            }
            return true;
        }
        return false;
    }
    private boolean hasObstacles(int row, int col, int row1, int col1, Figure[][] board) {
        if (row1 - row == 2) {
            if (col1 - col == 1) {
                if (board[row + 1][col] != null) {
                    return true;
                }
            } else if (col - col1 == 1) {
                if (board[row + 1][col] != null) {
                    return true;
                }
            }
        } else if (row - row1 == 2) {
            if (col1 - col == 1) {
                if (board[row - 1][col] != null) {
                    return true;
                }
            } else if (col - col1 == 1) {
                if (board[row - 1][col] != null) {
                    return true;
                }
            }
        } else if (col1 - col == 2) {
            if (row1 - row == 1) {
                if (board[row][col + 1] != null) {
                    return true;
                }
            } else if (row - row1 == 1) {
                if (board[row][col + 1] != null) {
                    return true;
                }
            }
        } else if (col - col1 == 2) {
            if (row1 - row == 1) {
                if (board[row][col - 1] != null) {
                    return true;
                }
            } else if (row - row1 == 1) {
                if (board[row][col - 1] != null) {
                    return true;
                }
            }
        }
        return false;
    }
}
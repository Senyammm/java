package Figures;

public class Queen extends Figure{
    public Queen(String name, char color, int row, int col) {
        super(name, color, row, col);
    }

    public boolean canMove(int row, int col, int row1, int col1, Figure[][] board) {
        if (row != row1 && col != col1 && Math.abs(row - row1) == Math.abs(col - col1)) {
            int deltaRow = (row1 - row) > 0 ? 1 : -1;
            int deltaCol = (col1 - col) > 0 ? 1 : -1;
            int x = row + deltaRow;
            int y = col + deltaCol;
            while (x != row1 && y != col1) {
                if (board[x][y] != null) {
                    return false;
                }
                x += deltaRow;
                y += deltaCol;
            }
            return true;
        }
        else if ((row == row1 && col != col1) || (row != row1 && col == col1)) {
            if (row == row1) {
                int deltaCol = (col1 - col) > 0 ? 1 : -1;
                int y = col + deltaCol;
                while (y != col1) {
                    if (board[row][y] != null) {
                        return false;
                    }
                    y += deltaCol;
                }
            } else {
                int deltaRow = (row1 - row) > 0 ? 1 : -1;
                int x = row + deltaRow;
                while (x != row1) {
                    if (board[x][col] != null) {
                        return false;
                    }
                    x += deltaRow;
                }
            }
            return true;
        }

        return false;
    }

    public boolean canAttack(int row, int col, int row1, int col1) {
        if (row == row1 || col == col1 || Math.abs(row - row1) == Math.abs(col - col1)) {
            return true;
        } else {
            return false;
        }
    }

}
package Figures;
public class King extends Figure{
    public King(String name, char color, int row, int col) {
        super(name, color, row, col);
    }
    public boolean canMove(int row1, int col1, Figure[][] location) {
        int rowDiff = Math.abs(this.getRow() - row1);
        int colDiff = Math.abs(this.getCol() - col1);

        return (rowDiff == 1 && this.getCol() == col1) ||
                (this.getRow() == row1 && colDiff == 1) ||
                (rowDiff == 1 && colDiff == 1);
    }
}